Bootstrap4 Alert Notification
=============================
Notification for Bootstrap 4 Alerts

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist cst/yii2-alert-bootstrap4-notify "*"
```

or
 
 ```
 composer require --prefer-dist cst/yii2-alert-bootstrap4-notify "*"
 ```

or add in you composer.json

```
"cst/yii2-alert-bootstrap4-notify": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \cst\notify\AlertNotify::widget(); ?>```