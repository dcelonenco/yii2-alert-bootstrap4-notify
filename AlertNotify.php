<?php

namespace cst\notify;

use Yii;
use yii\bootstrap4\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is just bootstrap 4 notification based on alert.
 */
class AlertNotify extends Widget
{
    /**
     * Info Alert Type
     */
    const _PRIMARY = 'primary';

    /**
     * Secondary Alert Type
     */
    const _SECONDARY = 'secondary';

    /**
     * Success Alert Type
     */
    const _SUCCESS = 'success';


    /**
     * Danger Alert Type
     */
    const _DANGER = 'danger';

    /**
     * Warning Alert Type
     */
    const _WARNING = 'warning';

    /**
     * Info Alert Type
     */
    const _INFO = 'info';

    /**
     * Light Alert Type
     */
    const _LIGHT = 'light';

    /**
     * Dark Alert Type
     */
    const _DARK = 'dark';

    /**
     * @var bool
     * All the messages stored in the session
     */
    public $useFlashSession = true;


    public function run()
    {
        parent::run();

        if ($this->useFlashSession) {
            $session = Yii::$app->getSession();
            $allFlashes = $session->getAllFlashes();
            foreach ($allFlashes as $alertType => $flash) {
                $notifyType = $this->getAlertType($alertType);
                if($notifyType){
                    if (ArrayHelper::isAssociative($flash)) {
                        $this->options = ArrayHelper::merge($this->options, $flash);
                        $this->clientOptions['type'] = $notifyType;
                        $this->generateNotifyBox();
                    } else {
                        $data = (array)$flash;
                        foreach ($data as $i => $message) {
                            $this->options['message'] = $message;
                            $this->clientOptions['type'] = $notifyType;
                            $this->generateNotifyBox();
                        }
                    }
//                    $this->options = [];
                    // clear the flash message
                    $session->removeFlash($alertType);
                }
            }
        } else {
            $this->generateNotifyBox();
        }
    }

    /**
     * Generate the notification box
     */
    protected function generateNotifyBox()
    {
        $view = $this->getView();
        RemarkableBootstrapNotifyAsset::register($view);
        $js = "$.notify({$this->getOptions()},{$this->getClientOptions()});";
        $view->registerJs($js, $view::POS_END);
    }

    /**
     * Get options in the json format
     *
     * @return string
     */
    protected function getOptions()
    {
        return Json::encode($this->options);
    }

    /**
     * Get client options in the json format
     *
     * @return string
     */
    protected function getClientOptions()
    {
        return Json::encode($this->clientOptions);
    }

    /**
     * Get the value of the constant alert type
     * @param $type
     * @return mixed|null
     */
    protected function getAlertType($type){
        $alertType = null;
        $const_type = '_' . strtoupper($type);
        if(constant("self::$const_type")){
            $alertType = constant("self::$const_type");
        }
        return $alertType;
    }
}
