<?php

namespace cst\notify;

use yii\web\AssetBundle;

/**
 * Class RemarkableBootstrapNotifyAsset
 * Remarkable bootstrap notify resource
 * @package cst\notify
 */
class RemarkableBootstrapNotifyAsset extends AssetBundle
{
    /**
     * @var string the directory that contains the source asset files for this asset bundle.
     */
    public $sourcePath = '@bower/remarkable-bootstrap-notify';

    /**
     * @var array
     */
    public $js = [
        'bootstrap-notify.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset', // using the basic Yii asset
        'yii\bootstrap4\BootstrapAsset', //using Bootstrap 4 asset
    ];
}
